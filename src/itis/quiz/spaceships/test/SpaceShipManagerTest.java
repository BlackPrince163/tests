package itis.quiz.spaceships.test;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;

import java.util.ArrayList;

public class SpaceShipManagerTest {

    SpaceshipFleetManager center;
    ArrayList<Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {
        double right = 0;
        double appraisal = 0.45;

        SpaceShipManagerTest spaceShipManagerTest = new SpaceShipManagerTest(new CommandCenter());

        boolean test1 = spaceShipManagerTest.getShipByName_shipExists_returnTargetShip();
        if (test1){
            System.out.println("Test 1 good!");
            right += appraisal;
        } else {
            System.out.println("Test 1 failed!");
        }

        boolean test2 = spaceShipManagerTest.getShipByName_shipNotExists_returnNull();
        if (test2){
            System.out.println("Test 2 good!");
            right += appraisal;
        } else {
            System.out.println("Test 2 failed!");
        }

        boolean test3 = spaceShipManagerTest.getAllShipsWightEnoughCargoSpace_returnsListSpaceshipWithWhichHasEnoughCargoSize();
        if (test3){
            System.out.println("Test 3 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 3 failed!");
        }

        boolean test4 = spaceShipManagerTest.getAllShipsWightEnoughCargoSpace_returnsEmptyList();
        if (test4){
            System.out.println("Test 4 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 4 failed!");
        }

        boolean test5 = spaceShipManagerTest.getAllCivilianShips_returnPeacefulShips();
        if (test5){
            System.out.println("Test 5 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 5 failed!");
        }

        boolean test6 = spaceShipManagerTest.getAllCivilianShips_returnEmptyList();
        if (test5){
            System.out.println("Test 6 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 6 failed!");
        }

        boolean test7 = spaceShipManagerTest.getMostPowerfulShip_returnTheMostHeavilyArmedShip();
        if (test7){
            System.out.println("Test 7 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 7 failed!");
        }

        boolean test8 = spaceShipManagerTest.getMostPowerfulShip_returnFirstOnTheList();
        if (test8){
            System.out.println("Test 8 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 8 failed!");
        }

        boolean test9 = spaceShipManagerTest.getMostPowerfulShip_returnNull();
        if (test9){
            System.out.println("Test 9 All good!");
            right += appraisal;
        } else {
            System.out.println("Test 9 failed!");
        }

        System.out.println(Math.floor(right));
    }


    private boolean getShipByName_shipExists_returnTargetShip(){

        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mark 1", 100, 0, 10));

        String testName = "Bar";
        Spaceship result = center.getShipByName(testList,testName);

        if (result != null && result.getName().equals(testName)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipNotExists_returnNull(){

        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mark 1", 100, 0, 10));

        String testName = "Mark 2";
        Spaceship result = center.getShipByName(testList, testName);

        if (result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getAllShipsWightEnoughCargoSpace_returnsListSpaceshipWithWhichHasEnoughCargoSize() {

        ArrayList<Spaceship> cargoShips = new ArrayList<>();

        cargoShips.add(new Spaceship("ship1", 10, 100, 10));
        cargoShips.add(new Spaceship("ship2", 10, 0, 10));
        cargoShips.add(new Spaceship("ship3", 10, 120, 10));

        Integer cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips, cargoSpace);

        for (Spaceship spaceship: result) {
            if (spaceship != null && spaceship.getCargoSpace() < cargoSpace){
                cargoShips.clear();
                return false;
            }

        }
        return true;
    }

    private boolean getAllShipsWightEnoughCargoSpace_returnsEmptyList(){
        ArrayList<Spaceship> cargoShips = new ArrayList<>();
        cargoShips.add(new Spaceship("ship1", 10, 30, 10));
        cargoShips.add(new Spaceship("ship2", 10, 0, 10));
        cargoShips.add(new Spaceship("ship3", 10, 99, 10));

        Integer cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips, cargoSpace);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean getAllCivilianShips_returnPeacefulShips(){
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        civilianShips.add(new Spaceship("ship1", 50, 100,10));
        civilianShips.add(new Spaceship("ship2", 0, 30,10));
        civilianShips.add(new Spaceship("ship3", 1, 140,10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShips);

        for (Spaceship spaceship : result) {
            if (spaceship != null && spaceship.getFirePower() == 0){
                civilianShips.clear();
                return true;
            }
        }
        civilianShips.clear();
        return false;
    }

    private boolean getAllCivilianShips_returnEmptyList(){
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        civilianShips.add(new Spaceship("ship1", 50, 100,10));
        civilianShips.add(new Spaceship("ship2", 10, 30,10));
        civilianShips.add(new Spaceship("ship3", 13, 140,10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShips);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean getMostPowerfulShip_returnTheMostHeavilyArmedShip(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 50, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 100, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 30, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);
        for (int i = 0; i < powerfulShip.size(); i++) {
            if (powerfulShip.get(i).getFirePower() < result.getFirePower()) {
                powerfulShip.clear();
                return true;
            }
        }
        powerfulShip.clear();
        return false;
    }

    private boolean getMostPowerfulShip_returnFirstOnTheList(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 50, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 100, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 100, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);

        if(!result.getName().equals(powerfulShip.get(0).getName())){
            return true;
        }
        return false;

    }

    private boolean getMostPowerfulShip_returnNull(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 0, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 0, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 0, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);

        if(result != null){
            return false;
        }
        return true;

    }

    public SpaceShipManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}
