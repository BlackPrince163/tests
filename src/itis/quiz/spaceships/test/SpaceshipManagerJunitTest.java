package itis.quiz.spaceships.test;
import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipManagerJunitTest {

    static SpaceshipFleetManager center = new CommandCenter();
    static ArrayList<Spaceship> testList = new ArrayList<>();

    @BeforeEach
    void listClear() {
        testList.clear();
    }

    @Test
    @DisplayName("Проверяет есть ли такое имя")
    void getShipByName_shipExists_returnTargetShip() {

        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mark 1", 100, 0, 10));

        String testName = "Bar";
        Spaceship result = center.getShipByName(testList, testName);

        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Проверяет возвращение null")
    void getShipByName_shipNotExists_returnNull() {
        testList.add(new Spaceship("Foo", 100, 0, 10));
        testList.add(new Spaceship("Bar", 100, 0, 10));
        testList.add(new Spaceship("Mark 1", 100, 0, 10));

        String testName = "Mark 2";
        Spaceship result = center.getShipByName(testList, testName);

        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Проверяет большой груз")
    void getAllShipsWightEnoughCargoSpace_returnsListSpaceshipWithWhichHasEnoughCargoSize() {

        ArrayList<Spaceship> cargoShips = new ArrayList<>();

        cargoShips.add(new Spaceship("ship1", 10, 100, 10));
        cargoShips.add(new Spaceship("ship2", 10, 0, 10));
        cargoShips.add(new Spaceship("ship3", 10, 120, 10));

        Integer cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips, cargoSpace);

        Assertions.assertEquals(2, result.size());
    }

    @Test
    @DisplayName("Проверяет пустой список кораблей с большим грузом")
    void getAllShipsWightEnoughCargoSpace_returnsEmptyList() {
        ArrayList<Spaceship> cargoShips = new ArrayList<>();
        cargoShips.add(new Spaceship("ship1", 10, 30, 10));
        cargoShips.add(new Spaceship("ship2", 10, 0, 10));
        cargoShips.add(new Spaceship("ship3", 10, 99, 10));

        Integer cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips, cargoSpace);

        Assertions.assertEquals(0, result.size());
    }

    @Test
    @DisplayName("Проверяет что возращает не пустой список мирных кораблей")
    void getAllCivilianShips_returnPeacefulShips() {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        civilianShips.add(new Spaceship("ship1", 50, 100, 10));
        civilianShips.add(new Spaceship("ship2", 0, 30, 10));
        civilianShips.add(new Spaceship("ship3", 1, 140, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShips);

        Assertions.assertEquals(1, result.size());
    }

    @Test
    @DisplayName("Проверяет что возращает пустой список мирных кораблей ")
    void getAllCivilianShips_returnEmptyList(){
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        civilianShips.add(new Spaceship("ship1", 50, 100,10));
        civilianShips.add(new Spaceship("ship2", 10, 30,10));
        civilianShips.add(new Spaceship("ship3", 13, 140,10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShips);
        Assertions.assertEquals(0,result.size());
    }

    @Test
    @DisplayName("Проверяет что возращает корабль")
    void getMostPowerfulShip_returnTheMostHeavilyArmedShip(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 50, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 100, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 30, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);
        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Проверяет что возращает первый по списку корабль")
    void  getMostPowerfulShip_returnFirstOnTheList(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 50, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 100, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 100, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);
        Assertions.assertEquals("ship2", result.getName());
    }

    @Test
    @DisplayName("Проверяет что возращает первый по списку корабль")
    void etMostPowerfulShip_returnNull(){
        ArrayList<Spaceship> powerfulShip = new ArrayList<>();
        powerfulShip.add(new Spaceship("ship1", 0, 100, 10));
        powerfulShip.add(new Spaceship("ship2", 0, 100, 10));
        powerfulShip.add(new Spaceship("ship3", 0, 100, 10));

        Spaceship result = center.getMostPowerfulShip(powerfulShip);

        Assertions.assertNull(result);

    }
}