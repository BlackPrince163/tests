package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship mostPowerful = null;
        for (int i = 0; i < ships.size(); i++) {
            Spaceship spaceship = ships.get(i);
            if (spaceship == null) {
                continue;
            }
            int spaceshipPower = spaceship.getFirePower();
            if (mostPowerful == null) {
                if (spaceshipPower > 0) {
                    mostPowerful = spaceship;
                } else {
                    continue;
                }
            } else if (spaceshipPower > 0 && spaceshipPower > mostPowerful.getFirePower()) {
                mostPowerful = spaceship;
            }
        }
        return mostPowerful != null ? mostPowerful : null;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        return ships.stream()
                .filter(spaceship -> spaceship.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> result = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if(ships.get(i).getCargoSpace() >= cargoSize) {
                result.add(ships.get(i));
            }
        }
        return result;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> result = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower() == 0) {
                result.add(ships.get(i));
            }
        }
        return result;
    }
}
